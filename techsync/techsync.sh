#!/bin/bash

#
#	techsync.sh
#	Willem Hunt
#	05/17/2018
#
#	Builds an rsync command and runs a watch of the source and destination
#	directory sizes while rsync copies data
#
#	Usage: techsync.sh [s] [d]
#	s: source file or directory
#	d: destination file or directory
#


#initial variables
args=("$@") # source, destination, interval
source=${args[0]}
destination=${args[1]}
done=0
percentFinished=0

#find home directory
directory=$(echo $(pwd) | sed 's/ /\\ /g' )

#get excludes
echo "Files to exclude (starting in root of source, separate with spaces):"
read excludes

#build rsync command
cmd="rsync -aP ${directory}/${source} ${directory}/${destination} "

x=0
for dir in $excludes
do
	cmd+=" --exclude=\""
	cmd+=$(echo $dir | sed 's/ /\\ /g')
	cmd+="\" "
	excludeSizes[$x]=$(eval du -sk ${directory}/${source}${dir})
	x=$((x+1))
done
cmd+=" --update"

echo

#verify rsync command
echo $cmd
echo "Is this command correct? (y/n): "
read input
correct="$(echo $input | head -c 1 | tr '[:upper:]' '[:lower:]')"
if [ $correct == "y" ] ; then


	echo

	#record directory sizes
	echo "Evaluating directory sizes..."
	sourceWatch="$(du -sk ${source})"
	destinationWatch="$(du -sk ${destination})"

	startTime="$(date -u +%s)"
	startClock="$(date +'%r')"

	#make and run temp file
	echo "cd $directory" >> tmp.sh
	echo  $cmd > tmp.sh
	chmod +x tmp.sh
	open -a Terminal.app tmp.sh

	echo

	echo "Source size: "
	echo "$sourceWatch"
	echo "Destination size: "
	echo "$destinationWatch"
	echo "Exclude Sizes:"

	toMove=$(eval du -shk ${source} | awk '{print $1}')
	for dush in "${excludeSizes[@]}"
	do
		echo "$dush"
		curDush=$(echo ${dush} | awk '{print $1}')
		toMove=$((toMove-curDush))
	done

	echo "To Move: $toMove"
	echo




	echo "Starting rsync..."
	sleep 1

	#start file watch
	while [ $done -eq 0 ]; do

		destinationSize="$(du -sk ${destination} | awk '{print $1}')"
		percentFinished=$(echo "100*($destinationSize/$toMove)" | bc -l)
		percentFinished=$(printf "%.02f" $percentFinished)
		clear
		echo "Source:"
		echo -e "\t$sourceWatch"

		echo "Destination:"
		echo -e "\t$destinationSize $destination"
		echo "Excludes:"
		for dush in "${excludeSizes[@]}"
		do
			echo -e "\t$dush"
		done
		echo
		
		currentTime="$(date -u +%s)"
		clockTime="printf '%(%I:%M %p)T\n'"
		elapsedTime="$(($currentTime-$startTime))"
		echo "Start Time: $startClock"
		echo "Current Time: $(date +'%r')"
		echo "Time Elapsed: $elapsedTime seconds"
		
		echo
		echo "Percent Finished: $percentFinished"

		progressBar="["
		counter=1
		while [ $counter -lt 50 ]; do
			pc=$(printf "%.0f" $(echo "$percentFinished/2" | bc))
			if [ $pc -ge $counter ]
			then
				progressBar="${progressBar}*"
			else
				progressBar="${progressBar}_"
			fi
			counter=$((counter+1))
		done
		progressBar="${progressBar}]"
		echo $progressBar
		echo
		
		
		
		ps cax | grep -w rsync >> /dev/null
		if [ $? -eq 1 ]; then
			done=1
		fi
		sleep 0.1
	done

	echo "rsync process finished, check other window for more details."
	rm tmp.sh
else
	echo "File transfer aborted."
fi
